FROM ubuntu:20.10

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y gpg curl nano htop mc systemctl

RUN curl https://download.jitsi.org/jitsi-key.gpg.key | sh -c 'gpg --dearmor > /usr/share/keyrings/jitsi-keyring.gpg'

RUN echo 'deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/' | tee /etc/apt/sources.list.d/jitsi-stable.list > /dev/null

RUN apt update && apt install -y jitsi-videobridge

RUN systemctl enable jitsi-videobridge.service
